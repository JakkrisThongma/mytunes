package com.noroff.MyTunes.models;

//Class for creating the Music objects which is asked for in part 1

public class Music {
    private String artists;
    private String songs;
    private String genre;
    private String album;

    public Music(String songs, String artists, String genre) {
        this.artists = artists;
        this.songs = songs;
        this.genre = genre;
    }

    public Music(String songs, String artists, String genre, String album) {
        this.artists = artists;
        this.songs = songs;
        this.genre = genre;
        this.album = album;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getAlbum() {
        return album;
    }

    public String getSongs() {
        return songs;
    }

    public void setSongs(String songs) {
        this.songs = songs;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
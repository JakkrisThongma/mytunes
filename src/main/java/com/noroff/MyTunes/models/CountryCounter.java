
package com.noroff.MyTunes.models;

/*
* This class is created to be able to create an object for counting the customers in each country
*/

public class CountryCounter {
    private String country;
    private int numOfCustomers;

    public CountryCounter(String country, int numOfCustomers) {
        this.country = country;
        this.numOfCustomers = numOfCustomers;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNumOfCustomers() {
        return numOfCustomers;
    }

    public void setNumOfCustomers(int numOfCustomers) {
        this.numOfCustomers = numOfCustomers;
    }
}

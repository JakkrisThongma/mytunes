package com.noroff.MyTunes.models;

//Class for creating an object with the name and the amount spent for a customer

public class CustomerSpender {
    private String firstName;
    private String lastName;
    private int spent;

    public CustomerSpender(String firstName, String lastName, int spent) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.spent = spent;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSpent() {
        return spent;
    }

    public void setSpent(int spent) {
        this.spent = spent;
    }
}

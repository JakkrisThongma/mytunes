package com.noroff.MyTunes.models;

//Class for creating object of a genre with the number of invoices to that genre.

public class CustomerGenre {

    private String mostPopularGenre;
    private int numOfInvoices;

    public CustomerGenre(String genreName, int numOfInvoices){
        this.mostPopularGenre = genreName;
        this.numOfInvoices = numOfInvoices;
    }

    public String getMostPopularGenre() {
        return mostPopularGenre;
    }

    public void setMostPopularGenre(String mostPopularGenre) {
        this.mostPopularGenre = mostPopularGenre;
    }

    public int getNumOfInvoices() {
        return numOfInvoices;
    }

    public void setNumOfInvoices(int numOfInvoices) {
        this.numOfInvoices = numOfInvoices;
    }
}

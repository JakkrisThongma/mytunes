package com.noroff.MyTunes.controllers;
import com.noroff.MyTunes.dataAccess.CustomerRepository;
import com.noroff.MyTunes.models.CountryCounter;
import com.noroff.MyTunes.models.Customer;
import com.noroff.MyTunes.models.CustomerGenre;
import com.noroff.MyTunes.models.CustomerSpender;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/*
* This controller class handles the endpoints from the API.
* It takes in the data collected in the CustomerRepository, and assigns the data to the right endpoint.
*/
@RestController
public class CustomerController {
    private CustomerRepository customerRepository = new CustomerRepository();

    //All the customers collected from the database
    @RequestMapping(value = "/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    //Add a customer to the database
    @RequestMapping(value = "/api/customers", method = RequestMethod.POST)
    public Boolean addCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    //Change the data of a customer, given its id
    @RequestMapping(value = "/api/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateCustomer(@PathVariable int id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

    //Lists of number of customers in each country
    @RequestMapping(value = "/api/customers/customersByCountry", method = RequestMethod.GET)
    public ArrayList<CountryCounter> getNumOfCustomersCountry(){
        return customerRepository.getNumberOfCustomers();
    }

    //Lists the highest spenders from the database
    @RequestMapping(value = "/api/customers/highestSpenders", method = RequestMethod.GET)
    public ArrayList<CustomerSpender> getHighestSpenders(){
        return customerRepository.getHighestSpenders();
    }

    //Lists out the most popular genre for a customer, given its id.
    //Is a list because a customer can have several genres which is most popular
    @RequestMapping(value = "/api/customers/{id}/popularGenre", method = RequestMethod.GET)
    public ArrayList<CustomerGenre> mostPopularGenres(@PathVariable int id){
        return customerRepository.mostPopularGenreCustomer(id);
    }
}

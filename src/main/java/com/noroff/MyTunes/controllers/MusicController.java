package com.noroff.MyTunes.controllers;
import com.noroff.MyTunes.dataAccess.MusicRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/*
 * This controller class handles the endpoints from the music part.
 * It takes in the data collected in the MusicRepository, and adds them to the right attribute, which is
 * used in the Thymeleaf templates.
 */
@Controller
public class MusicController {
    MusicRepository musicRepository = new MusicRepository();

    //Takes in a list with 5 random songs, artists and genres, which are getting displayed in
    //the index.html template.
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getRandomMusic(Model model){
        model.addAttribute("music", musicRepository.getRandomMusic());
        return "index";
    }

    //Searches for a song and the results is displayed in the search-result.html template.
    //It takes in an arraylist with the results.
    @GetMapping(value = "/search-result")
    public String getMusic(@RequestParam(defaultValue = "") String track, Model model){
        model.addAttribute("search", track);
        model.addAttribute("tracks", musicRepository.getMusic(track));
        return "search-result";
    }

}

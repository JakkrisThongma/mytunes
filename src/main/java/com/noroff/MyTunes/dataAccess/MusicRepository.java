package com.noroff.MyTunes.dataAccess;

import com.noroff.MyTunes.models.Music;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class MusicRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    //Method that populates the home view(index). Method returns a list of 5 random songs, 5 random artists and 5 random genre.
    public ArrayList<Music> getRandomMusic(){
        ArrayList<Music> music = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connected to DB!");

            //3 separete PreparedStatements that returns 5 random values from name row from each table.
            PreparedStatement preparedStatement1 = conn.prepareStatement(
                    "SELECT t.Name FROM Track t ORDER BY RANDOM() LIMIT 5"
            );
            PreparedStatement preparedStatement2 = conn.prepareStatement(
                    "SELECT a.Name FROM Artist a ORDER BY RANDOM() LIMIT 5"
            );
            PreparedStatement preparedStatement3 = conn.prepareStatement(
                    "SELECT g.Name FROM Genre g ORDER BY RANDOM() LIMIT 5"
            );

            //Execute the statements
            ResultSet resultSet1 = preparedStatement1.executeQuery();
            ResultSet resultSet2 = preparedStatement2.executeQuery();
            ResultSet resultSet3 = preparedStatement3.executeQuery();

            //Adds results to arraylist.
            while (resultSet1.next() && resultSet2.next() && resultSet3.next()){
                music.add(new Music(
                        resultSet1.getString("Name"),
                        resultSet2.getString("Name"),
                        resultSet3.getString("Name")
                ));
            }
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }//Close DB connection
        finally {
            try {
                conn.close();
            } catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
        return music;
    }

    //Method that returns arraylist with values that matches input(SearchString)
    public ArrayList<Music> getMusic(String searchString){
        ArrayList<Music> music = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connected to DB!");

            //PreparedStatement that selects trackname, artistname, genrename and albumtitle if searchstring matches trackname.
            //Like in SQL query so that it finds matches so long as the sequence of characters matches, therefore case insensitive.
            PreparedStatement preparedStatement1 = conn.prepareStatement(
                    "SELECT t.Name AS TrackName, a.Name AS ArtistName, g.Name AS GenreName, al.Title FROM genre g JOIN track t ON g.GenreId = t.GenreId JOIN album al ON t.AlbumId = al.AlbumId JOIN artist a ON al.ArtistId = a.ArtistId WHERE TrackName LIKE ?"
            );
            preparedStatement1.setString(1, "%" + searchString + "%");

            //Executes the query
            ResultSet resultSet = preparedStatement1.executeQuery();

            //Adds results to arraylist
            while (resultSet.next()){
                music.add(new Music(
                        resultSet.getString("TrackName"),
                        resultSet.getString("ArtistName"),
                        resultSet.getString("GenreName"),
                        resultSet.getString("Title")
                )
                );
            }
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }//Close DB connection
        finally {
            try {
                conn.close();
            } catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
        return music;
    }
}

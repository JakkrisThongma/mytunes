package com.noroff.MyTunes.dataAccess;

import com.noroff.MyTunes.models.CountryCounter;
import com.noroff.MyTunes.models.Customer;
import com.noroff.MyTunes.models.CustomerGenre;
import com.noroff.MyTunes.models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class CustomerRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    // Method that queries the DB to get out information about all the customers. Returns an ArrayList than contains Customer objects.
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            //DB connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connected to DB!");

            //PreparedStatement where the SQL query is defined.
            //Sql query selects customerId, firstname, lastname, postalcode, country, phone and email.
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, PostalCode, Country, Phone, Email FROM customer");
            ResultSet resultSet = preparedStatement.executeQuery();

            //While loop running while resultSet has next result from the DB. Creates customer object, adds the fields retrieved from DB to constructor.
            while (resultSet.next()){
                customers.add(
                        new Customer(
                                resultSet.getInt("customerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Country"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        )
                );
            }
            System.out.println("Select all customers was successful!");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }//Close DB connection
        finally {
            try {
                conn.close();
            } catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
        return customers;
    }

    //Boolean method that takes in a customer and tries to add it to DB.
    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try{
            //DB connection
            conn = DriverManager.getConnection(URL);
            System.out.println("connected to DB");

            //PreparedStatement where the SQL query is defined.
            //Sql query creates customer and sets values.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?,?,?,?,?,?,?) ");
            preparedStatement.setInt(1,customer.getId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getCountry());
            preparedStatement.setString(5,customer.getPostalCode());
            preparedStatement.setString(6,customer.getPhoneNumber());
            preparedStatement.setString(7,customer.getEmail());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            System.out.println("Customer added to DB");
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }//Close DB connection
        finally {
            try{
                conn.close();
            }
            catch(Exception exception)
            {
                System.out.println(exception.getMessage());
            }
        }
        return success;
    }

    //Boolean method that tries to update customer in the DB.
    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try {
            //DB connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connected to DB!");

            //PreparedStatement where the SQL query is defined.
            //Sql query updates customer with values where customerId matches with given input.
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "UPDATE customer SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?");

            //Binds the variables from customer to the SQL query
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, customer.getId());

            //Check to see if the query was successfull, in other words if customer got updated in DB.
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            System.out.println("Update customer was successful");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }//Close the DB connection
        finally {
            try {
                conn.close();
            } catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
        return success;
    }

    //Method that returns number of customers in each country.
    public ArrayList<CountryCounter> getNumberOfCustomers(){
        ArrayList<CountryCounter> numOfCustomers = new ArrayList<>();
        try {
            //DB connecton
            conn = DriverManager.getConnection(URL);
            System.out.println("Connected to DB!");

            //PreparedStatement where SQL query is defined
            //Sql query select country and counts how many customerId exists for that specific country. Order by number of customers a country has descending.
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Country, COUNT (CustomerId) AS NumOfCostumers FROM customer GROUP BY Country ORDER BY NumOfCostumers DESC ");
            ResultSet resultSet = preparedStatement.executeQuery();

            //Adds results to a arraylist
            while (resultSet.next()){
                numOfCustomers.add(
                        new CountryCounter(resultSet.getString("Country"),
                                resultSet.getInt("NumOfCostumers"))
                );
            }
            System.out.println("Num of costumers works");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }//Close the DB connection
        finally {
            try {
                conn.close();
            } catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
        return numOfCustomers;
    }

    //Method that returns arraylist with highest spenders.
    public ArrayList<CustomerSpender> getHighestSpenders(){
        ArrayList<CustomerSpender> spent = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connected to DB!");
            //PreparedStatement where Sql query is defined.
            //Sql query selects firstname, lastname from customer and finds total from invoice where customer.id = invoice.customerid. Then orders this by total descending.
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "SELECT FirstName, LastName, Total FROM customer JOIN invoice ON customer.CustomerId = invoice.CustomerId ORDER BY Total DESC"
            );
            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            //Adds results to arraylist
            while (resultSet.next()){
                spent.add(new CustomerSpender(
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getInt("Total")
                ));
            }
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }//Close DB connection
        finally {
            try {
                conn.close();
            } catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
        return spent;
    }

    //Method that returns most popular genre for a given customer.
    public ArrayList<CustomerGenre> mostPopularGenreCustomer(int id){
        ArrayList<CustomerGenre> customerGenre = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connected to DB!");
            //PreparedStatement where Sql query is defined.
            PreparedStatement ps = conn.prepareStatement(
                    "WITH popular_genre AS (SELECT g.name Name, COUNT(g.name) NumOfHits FROM invoice i " +
                            "JOIN invoiceline il ON il.invoiceid = i.invoiceid JOIN track t ON il.trackId = t.trackId " +
                            "JOIN genre g ON t.genreId = g.genreId WHERE i.customerid = ? GROUP BY  g.name ORDER BY  NumOfHits desc) " +
                            "SELECT Name, NumOfHits  FROM popular_genre WHERE NumOfHits = (SELECT MAX(NumOfHits) FROM popular_genre)"
            );

            ps.setInt(1, id);

            //Execute query
            ResultSet resultSet = ps.executeQuery();

            //Adds results to arraylist
            while (resultSet.next()){
                customerGenre.add(
                        new CustomerGenre(
                                resultSet.getString("Name"),
                                resultSet.getInt("NumOfHits")
                        )
                );
            }
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }//Close DB connection
        finally {
            try {
                conn.close();
            } catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
        return customerGenre;
    }
}

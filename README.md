# Assignment 4: MyTunes

## The task
The task of this assignment was to create a Spring boot application, using the Chinook database. This assignment is divided into two parts; one for handling the songs in the database and one for creating a REST API, handling the customers.

## Run the application
The link to the application is: https://assignment4mytunes.herokuapp.com/

## Contributors
Jakkris Thongma and Markus Thomassen Koteng

## The application
It is created a Docker application from the Spring boot application.
### Back-end
The data is gathered from the Chinook database. A schema of the database can be found on https://www.codestencil.com/database/chinook-database-schema. The application is a Spring boot application, which creates endpoints for exposing the data requested from the assignment. 
The API created uses RestController from the Spring boot framework to handle requests. Postman is used to make requests to the API, and a collection with requests can be found in the postman folder The controllers gets its data from different repositories. These repositories is using JDBC to make requests directly to the database. 
#### Postman
In the postman directory, a collection can be found, with the needed requests to the endpoints created for part 2 of the assignment. This can be imported into Postman, where you can do the needed requests to the endpoints.
#### The API endpoints
##### /api/customers
This endpoint is fetching all the customers in the database, with the GET method. It is also possible to make a POST request to insert a new customer into the database.
##### /api/customers/{id}
This endpoint gives the posibillity to change the data of a given customer, by using the PUT method. 
##### /api/customers/customersByCountry
This endpoint is fetching a list whith the number of customers for each country, with the GET method. The list is sorted descending, displaying the countries with the highest amount of customers first. 
##### /api/customers/highestSpenders
This endpoint is returning a list wiith the highest spenders which is based on the total in the invoice table, using the GET method. The list is ordered descending.
##### /api/customers/{id}/popularGenre
This endpoint is fetching themost popular genre for a customer, with the GET method. The most popular genre is the most tracks from invoices assosiated to that customer.
### Front-end
Thymeleaf was used for part one of the assignment. It was used to make a template where the user can see songs, artists and genre from the database. The main site is collecting 5 random songs, artists and genres. Everytime the site is refreshed a new randomized table is exposed. It also have the posibillity to search for songs in the database, where the user can search for a word in the title, or the total title. The search hanndles both uppercase annd lowercase letters.

FROM openjdk:15
ADD target/MyTunes-0.0.1-SNAPSHOT.jar myTunes.jar
ENTRYPOINT ["java", "-jar", "/myTunes.jar"]